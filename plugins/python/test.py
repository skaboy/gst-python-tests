import gi
gi.require_version('GstBase', '1.0')

from gi.repository import Gst, GObject, GstBase
Gst.init(None)

class IdentityProperty(GstBase.BaseTransform):
    __gstmetadata__ = ('Identity Python','Transform',
                      'Simple identity element written in python', 'Marianna S. Buschle')

    __gsttemplates__ = (Gst.PadTemplate.new("src",
                                           Gst.PadDirection.SRC,
                                           Gst.PadPresence.ALWAYS,
                                           Gst.Caps.new_any()),
                       Gst.PadTemplate.new("sink",
                                           Gst.PadDirection.SINK,
                                           Gst.PadPresence.ALWAYS,
                                           Gst.Caps.new_any()))

    __gproperties__ = {
        'mode': (str, 'mode', '', "", GObject.ParamFlags.READWRITE)}

    def do_transform_ip(self, buffer):
        return Gst.FlowReturn.OK

    def do_set_property(self, prop, value):
        if prop.name == 'mode':
            pass
        else:
            raise AttributeError('unknown property %s' % prop.name)

    def do_get_property(self, prop):
        if prop.name == 'mode':
            return "test"
        else:
            raise AttributeError('unknown property %s' % prop.name)


GObject.type_register(IdentityProperty)
__gstelementfactory__ = ("identity_property_py", Gst.Rank.NONE, IdentityProperty)
