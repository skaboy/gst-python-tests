#!/usr/bin/env sh

export GST_PLUGIN_PATH=$GST_PLUGIN_PATH:$PWD/plugins

echo "OK"
gst-launch-1.0 fakesrc num-buffers=10 ! identity_property_py ! fakesink

echo "FAIL"
gst-launch-1.0 fakesrc num-buffers=10 ! identity_property_py mode=test ! fakesink
